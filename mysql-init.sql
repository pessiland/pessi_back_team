CREATE TABLE type (
                      id_type INT AUTO_INCREMENT PRIMARY KEY,
                      nom_type VARCHAR(45)
);

-- Table Rarete
CREATE TABLE rarete (
                        id_rarete INT AUTO_INCREMENT PRIMARY KEY,
                        nom_rarete VARCHAR(45)
);

-- Table Spessimen
CREATE TABLE spessimen (
                           id_spessimen INT AUTO_INCREMENT PRIMARY KEY,
                           nom VARCHAR(45),
                           type INT,
                           pv INT,
                           rarete INT,
                           description VARCHAR(1000),
                           FOREIGN KEY (type) REFERENCES type(id_type),
                           FOREIGN KEY (rarete) REFERENCES rarete(id_rarete)
);


-- Remplir la table Rarete
INSERT INTO rarete (nom_rarete) VALUES ('Common'), ('Rare'), ('Epic'), ('Legendary');

-- Remplir la table Type
INSERT INTO type (nom_type) VALUES ('Goatesque'), ('Feur'), ('Irfane'), ('Penaldo');

-- Remplir la table Spessimen
INSERT INTO spessimen (nom, type, pv, rarete, description)
VALUES
    ('Pechat', 2, 80, 1, 'Pechat'),
    ('Peshy', 1, 76, 1, 'Peshy'),
    ('Pessi géant électrique', 3, 143, 4, 'Pessi géant électrique'),
    ('Pessing Bad', 1, 98, 3, 'Pessing Bad'),
    ('Pessi Kong', 2, 84, 2, 'Pessi Kong'),
    ('Dripessi', 4, 93, 3, 'Dripessi'),
    ('Pessi de la liberté', 3, 58, 1, 'Pessi de la liberté'),
    ('Pessi sauteuse', 1, 69, 2, 'Pessi sauteuse'),
    ('Pessi guidant le peuple', 4, 58, 1, 'Pessi guidant le peuple'),
    ('Pessitroën', 3, 100, 3, 'Pessitroën'),
    ('Pessitron', 4, 111, 4, 'Pessitron'),
    ('Plash', 1, 97, 3, 'Plash'),
    ('Variant pessi', 2, 66, 1, 'Variant pessi'),
    ('Pessi coleman', 2, 70, 2, 'Pessi coleman'),
    ('Pessi hallyday', 3, 74, 1, 'Pessi hallyday'),
    ('Peteuf', 4, 88, 2, 'Peteuf'),
    ('Pessi professeur', 2, 49, 1, 'Pessi professeur'),
    ('Hello pessi', 1, 121, 4, 'Hello pessi'),
    ('Pevil man', 3, 56, 1, 'Pevil man'),
    ('Pessimitri payet', 1, 79, 2, 'Pessimitri payet'),
    ('Pessidré', 4, 55, 1, 'Pessidré'),
    ('Pessycliste', 1, 66, 1, 'Pessycliste'),
    ('Pessirfan', 3, 10, 1, 'Pessirfan');
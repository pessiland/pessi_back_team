package com.project.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class RedisService {
    private final HashOperations<String, String, String> hashOperations;

    @Autowired
    public RedisService(RedisTemplate<String, String> redisTemplate) {
        this.hashOperations = redisTemplate.opsForHash();
    }

    public void savePlayerSpessimenAssociation(int playerId, int spessimenId) {
        String key = String.valueOf(playerId);
        String currentValue = hashOperations.get("player_spessimen", key);
        String newValue;
        if (currentValue != null) {
            newValue = currentValue + "," + spessimenId;
        } else {
            newValue = String.valueOf(spessimenId);
        }
        hashOperations.put("player_spessimen", key, newValue);
    }


    public String getSpessimenIdByPlayerId(String playerId) {
        return hashOperations.get("player_spessimen", playerId);
    }

    public Map<String, String> getAllPlayerSpessimenAssociations() {
        return hashOperations.entries("player_spessimen");
    }

    public void removeSpessimenFromTeam(String playerId, String spessimenId) {
        String currentSpessimenIds = getSpessimenIdByPlayerId(playerId);

        if (currentSpessimenIds != null && !currentSpessimenIds.isEmpty()) {
            List<String> currentIdsList = Arrays.asList(currentSpessimenIds.split(","));

            currentIdsList.remove(spessimenId);

            String updatedSpessimenIds = String.join(",", currentIdsList);

            updatePlayerSpessimenAssociation(playerId, updatedSpessimenIds);
        }
    }

    public void updatePlayerSpessimenAssociation(String playerId, String spessimenIds) {
        hashOperations.put("player_spessimen", playerId, spessimenIds);
    }

}

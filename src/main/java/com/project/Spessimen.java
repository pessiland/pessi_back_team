package com.project;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Spessimen {
    @JsonProperty("idSpessimen")
    private int idSpessimen;

    @JsonProperty("Nom")
    private String name;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("PV")
    private int pv;

    @JsonProperty("Rarete")
    private String rarete;

    @JsonProperty("Description")
    private String description;

    public int getIdSpessimen() {
        return this.idSpessimen;
    }

    public String getNom() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public int getPV() {
        return this.pv;
    }

    public String getRarete() {
        return this.rarete;
    }

    public String getDescription() {
        return this.description;
    }
}


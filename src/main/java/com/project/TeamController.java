package com.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/team")
public class TeamController {

    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping("/add")
    public void addSpessimenToTeam(@RequestHeader int playerId, @RequestParam int spessimenId) {
        teamService.addSpessimenToTeam(playerId, spessimenId);
    }

    @PostMapping("/remove")
    public void removeSpessimenFromTeam(@RequestHeader int playerId, @RequestParam String spessimenId) {
        teamService.removeSpessimenFromTeam(playerId, spessimenId);
        if (teamService.getTeamSize(playerId) == TeamService.MAX_TEAM_SIZE - 1) {
            teamService.handleDeadLetterQueueSynchronously();
        }
    }

    @PostMapping("/")
    public List<Spessimen> getMyTeam(@RequestHeader int playerId) {
        return teamService.getMyTeam(playerId);
    }
}

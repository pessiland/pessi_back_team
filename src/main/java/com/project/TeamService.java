package com.project;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.redis.RedisService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TeamService {

    public static final int MAX_TEAM_SIZE = 6;

    private final AmqpTemplate rabbitTemplate;

    private final RedisService redisService;

    private static final String SPESSIDEX_URL = "http://localhost:8081/spessidex/";

    @Autowired
    public TeamService(RedisService redisService, AmqpTemplate rabbitTemplate) {
        this.redisService = redisService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @RabbitListener(queues = "team-queue")
    public void handleSpessimenMessage(String jsonSpessimen, @Header("playerId") String playerId) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Spessimen spessimen = objectMapper.readValue(jsonSpessimen, Spessimen.class);

            if (getTeamSize(Integer.parseInt(playerId)) < MAX_TEAM_SIZE) {
                addSpessimenToTeam(Integer.parseInt(playerId), spessimen.getIdSpessimen());
            } else {
                rabbitTemplate.convertAndSend("dead-letter-exchange", "dead-letter", jsonSpessimen);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    public List<Spessimen> getMyTeam(int playerId) {
        String teamSpessimensString = redisService.getSpessimenIdByPlayerId(String.valueOf(playerId));

        List<Integer> spessimenIds = new ArrayList<>();
        if (teamSpessimensString != null && !teamSpessimensString.isEmpty()) {
            String[] spessimenIdsArray = teamSpessimensString.split(",");
            for (String idStr : spessimenIdsArray) {
                spessimenIds.add(Integer.parseInt(idStr.trim()));
            }
        }

        List<Spessimen> teamSpessimens = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        for (Integer spessimenId : spessimenIds) {
            String url = SPESSIDEX_URL + spessimenId;
            Object spessimenDetails = restTemplate.getForObject(url, Object.class);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Spessimen spessimen = objectMapper.convertValue(spessimenDetails, Spessimen.class);
            teamSpessimens.add(spessimen);
        }

        return teamSpessimens;
    }


    public void addSpessimenToTeam(int playerId, int spessimenId) {
        redisService.savePlayerSpessimenAssociation(playerId, spessimenId);
    }

    public void removeSpessimenFromTeam(int playerId, String spessimenId) {
        redisService.removeSpessimenFromTeam(String.valueOf(playerId), spessimenId);
        // Faire une requête POST vers le service Stockage pour libérer une place
    }

    public int getTeamSize(int userId) {
        Map<String, String> teamMembers = redisService.getAllPlayerSpessimenAssociations();

        long count = teamMembers.values().stream()
                .filter(spessimenId -> spessimenId.equals(String.valueOf(userId)))
                .count();

        return (int) count;
    }

    public void handleDeadLetterQueueSynchronously() {
        String deadLetterQueueName = "dead-letter-queue";

        Message message = rabbitTemplate.receive(deadLetterQueueName);

        if (message != null) {
            try {
                Spessimen spessimen = (Spessimen) new SimpleMessageConverter().fromMessage(message);
                addSpessimenToTeam(0, spessimen.getIdSpessimen());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

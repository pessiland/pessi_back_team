package com.project.messaging;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Component;

@Component
public class DeadLetterListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        // Logique de traitement des messages morts ici
    }
}
